package com.sankettask2aidl.activites;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.sankettask2aidl.Utils.Orientation;
import com.sankettask2aidl.R;

public class MainActivity extends AppCompatActivity implements Orientation.Listener {
    private Orientation mOrientation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    public void init(){
        mOrientation = new Orientation(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mOrientation.startListening(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOrientation.stopListening();
    }
    public void broadcastIntent(float pitch, float roll){
        Intent intent = new Intent();
        intent.setAction("com.data");
        intent.putExtra("pitch", pitch);
        intent.putExtra("roll", roll);
        sendBroadcast(intent);
    }
    @Override
    public void onOrientationChanged(float pitch, float roll) {
        broadcastIntent(pitch,roll);
    }
}