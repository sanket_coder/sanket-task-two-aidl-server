package com.sankettask2aidl.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.RemoteException;

import com.sankettask2aidl.IMyAidlInterface;

public class MyService extends Service {
    static float pitch,roll;
    UpdateReceiver updateReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        registerBroadcast();
        return aidlInterface;
    }

    private final IMyAidlInterface.Stub aidlInterface = new IMyAidlInterface.Stub() {
        @Override
        public float getPitch() throws RemoteException {
            return pitch;
        }
        @Override
        public float getRoll() throws RemoteException {
            return roll;
        }
    };

    static class UpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            pitch = intent.getExtras().getFloat("pitch",0f);
            roll = intent.getExtras().getFloat("roll",0f);
        }
    }

    public void registerBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.data");
        updateReceiver = new UpdateReceiver();
        registerReceiver(updateReceiver, filter);
    }
}
