// IMyAidlInterface.aidl
package com.sankettask2aidl;

// Declare any non-default types here with import statements

interface IMyAidlInterface {
   float getPitch();
   float getRoll();
}
